<?php
/**
 * Created by dh2y.
 * Blog: http://blog.csdn.net/sinat_22878395
 * Date: 2018/5/2 0002 18:34
 * For: 短信验证码配置
 */

return [
    'IS_TEST' => true,             //是否测试默认验证码 111111
    //短信基本配置
    'SMS_SDK' =>[
        'class' => 'Jianzhou',     //服务商
        'account' => 'sdk_dh2y',   //服务商账户(这里的key值可以根据服务商而定不一定是account)
        'password'=> 'dh2y',       //服务商密码(这里的key值可以根据服务商而定不一定是password)
        'signature' => '【XXXX】'   //签名
    ],

    //验证码使用场景文案
    'SMS_SCENE' =>[
        'register' => '注册验证码：%code%，有效时间5分钟，为保护您的账号安全，验证短信请勿泄露给其他人。',
        'retrieve' => '找回密码验证码：%code%，有效时间5分钟，为保护您的账号安全，验证短信请勿泄露给其他人。',
        'changePhone' =>'修改手机验证码：%code%，有效时间1分钟，为保护您的账号安全，验证短信请勿泄露给其他人。',
        'common' => '您的验证码是：%code%，有效时间5分钟，为保护您的账号安全，验证短信请勿泄露给他人。'  //普通短信验证码场景
    ]
];