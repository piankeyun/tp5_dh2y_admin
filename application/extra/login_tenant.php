<?php
/**
 * Created by dh2y.
 * Blog: http://blog.csdn.net/sinat_22878395
 * Date: 2018/4/28 0028 19:47
 * For:登录配置
 */
return [
    'crypt' => 'dh2y',      //Crypt加密秘钥
    'auth_uid' => 'authTenant',      //用户认证识别号(必配)
    'not_auth_module' => 'home,index', // 无需认证模块
    'user_auth_gateway' => 'home/index', // 默认网关

    'scene'     =>   'username|phone'    // 'username' 用户名登录 'phone' 手机号登录   'username|phone'用户名或者手机号登录
];
