/*
Navicat MySQL Data Transfer

Source Server         : lamp
Source Server Version : 50505
Source Host           : 192.168.88.90:3306
Source Database       : tp5_dh2y_admin

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-05-08 12:03:47
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `dh2y_admin`
-- ----------------------------
DROP TABLE IF EXISTS `dh2y_admin`;
CREATE TABLE `dh2y_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(64) NOT NULL,
  `token` varchar(32) NOT NULL,
  `email` varchar(128) DEFAULT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '{1:开启,0:关闭}',
  `last_login` int(11) DEFAULT '0' COMMENT '最后登录时间',
  `login_ip` varchar(126) DEFAULT '' COMMENT '登录ip',
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='管理员';

-- ----------------------------
-- Records of dh2y_admin
-- ----------------------------
INSERT INTO `dh2y_admin` VALUES ('1', 'admin', 'asdad', 'sdad', null, '1', null, null, '0', '0');
INSERT INTO `dh2y_admin` VALUES ('2', 'dh2y', '5be938c5a9ee41c9f74ebbe3e47fbe71', 'dh2y', 'qeq@qq.com', '1', '1525747735', '192.168.88.1', '0', '0');

-- ----------------------------
-- Table structure for `dh2y_auth_group`
-- ----------------------------
DROP TABLE IF EXISTS `dh2y_auth_group`;
CREATE TABLE `dh2y_auth_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(100) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `rules` char(80) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dh2y_auth_group
-- ----------------------------
INSERT INTO `dh2y_auth_group` VALUES ('1', '超级管理员', '1', '39,38,37,36,27,29,30,31,41,40,42,43,26,34,33,35,32');
INSERT INTO `dh2y_auth_group` VALUES ('3', '测试', '1', '39,38,37,36,41,42,43,26,27,29,31,34,35,33,32');
INSERT INTO `dh2y_auth_group` VALUES ('4', '测试去', '1', '39,38,37,36,43,42,41,40,26,27,29,30,31,35,34,33,32');

-- ----------------------------
-- Table structure for `dh2y_auth_group_access`
-- ----------------------------
DROP TABLE IF EXISTS `dh2y_auth_group_access`;
CREATE TABLE `dh2y_auth_group_access` (
  `uid` mediumint(8) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dh2y_auth_group_access
-- ----------------------------
INSERT INTO `dh2y_auth_group_access` VALUES ('1', '1');
INSERT INTO `dh2y_auth_group_access` VALUES ('2', '1');

-- ----------------------------
-- Table structure for `dh2y_auth_rule`
-- ----------------------------
DROP TABLE IF EXISTS `dh2y_auth_rule`;
CREATE TABLE `dh2y_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(80) NOT NULL DEFAULT '',
  `title` char(20) NOT NULL DEFAULT '',
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `condition` char(100) NOT NULL DEFAULT '',
  `mid` int(8) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dh2y_auth_rule
-- ----------------------------
INSERT INTO `dh2y_auth_rule` VALUES ('43', 'admin/menu/remove', '删除', '1', '1', '', '24');
INSERT INTO `dh2y_auth_rule` VALUES ('42', 'admin/menu/edit', '编辑', '1', '1', '', '24');
INSERT INTO `dh2y_auth_rule` VALUES ('41', 'admin/menu/add', '添加', '1', '1', '', '24');
INSERT INTO `dh2y_auth_rule` VALUES ('40', 'admin/menu/index', '菜单列表', '1', '1', '', '24');
INSERT INTO `dh2y_auth_rule` VALUES ('39', 'admin/auth/remove_group', '删除', '1', '1', '', '23');
INSERT INTO `dh2y_auth_rule` VALUES ('38', 'admin/auth/edit_group', '编辑', '1', '1', '', '23');
INSERT INTO `dh2y_auth_rule` VALUES ('37', 'admin/auth/add_group', '添加', '1', '1', '', '23');
INSERT INTO `dh2y_auth_rule` VALUES ('26', 'admin/admin/index', '列表', '1', '1', '', '26');
INSERT INTO `dh2y_auth_rule` VALUES ('27', 'admin/admin/add', '添加', '1', '1', '', '26');
INSERT INTO `dh2y_auth_rule` VALUES ('29', 'admin/admin/edit', '编辑', '1', '1', '', '26');
INSERT INTO `dh2y_auth_rule` VALUES ('30', 'admin/admin/remove', '删除', '1', '1', '', '26');
INSERT INTO `dh2y_auth_rule` VALUES ('31', 'admin/admin/status', '状态更改', '1', '1', '', '26');
INSERT INTO `dh2y_auth_rule` VALUES ('35', 'admin/auth/edit_access', '修改', '1', '1', '', '27');
INSERT INTO `dh2y_auth_rule` VALUES ('34', 'admin/auth/add_access', '添加', '1', '1', '', '27');
INSERT INTO `dh2y_auth_rule` VALUES ('33', 'admin/auth/remove', '删除', '1', '1', '', '27');
INSERT INTO `dh2y_auth_rule` VALUES ('32', 'admin/auth/index', '权限列表', '1', '1', '', '27');
INSERT INTO `dh2y_auth_rule` VALUES ('36', 'admin/auth/group_list', '角色列表', '1', '1', '', '23');

-- ----------------------------
-- Table structure for `dh2y_menu`
-- ----------------------------
DROP TABLE IF EXISTS `dh2y_menu`;
CREATE TABLE `dh2y_menu` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `pid` smallint(6) NOT NULL DEFAULT '0',
  `title` varchar(20) NOT NULL COMMENT '菜单名称',
  `link` varchar(50) DEFAULT NULL COMMENT '链接',
  `icon` varchar(50) DEFAULT NULL COMMENT '字体图标',
  `sort` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COMMENT='菜单表';

-- ----------------------------
-- Records of dh2y_menu
-- ----------------------------
INSERT INTO `dh2y_menu` VALUES ('1', '0', '权限管理', '#', 'Hui-iconfont-menu', '1');
INSERT INTO `dh2y_menu` VALUES ('2', '0', '系统管理', '', 'Hui-iconfont-manage', '2');
INSERT INTO `dh2y_menu` VALUES ('23', '1', '角色管理', '/admin/auth/group_list', '', '1');
INSERT INTO `dh2y_menu` VALUES ('24', '1', '菜单模块', '/admin/menu/index', '', '2');
INSERT INTO `dh2y_menu` VALUES ('26', '1', '管理员列表', '/admin/admin/index', '', '1');
INSERT INTO `dh2y_menu` VALUES ('27', '1', '权限列表', '/admin/auth/index', '', '1');

-- ----------------------------
-- View structure for `dh2y_admin_group_view`
-- ----------------------------
DROP VIEW IF EXISTS `dh2y_admin_group_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `dh2y_admin_group_view` AS select `a`.`username` AS `username`,`g`.`uid` AS `uid`,`g`.`group_id` AS `group_id` from (`dh2y_admin` `a` left join `dh2y_auth_group_access` `g` on((`a`.`id` = `g`.`uid`))) ;

-- ----------------------------
-- View structure for `dh2y_rule_menu_view`
-- ----------------------------
DROP VIEW IF EXISTS `dh2y_rule_menu_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `dh2y_rule_menu_view` AS select `r`.`id` AS `id`,`r`.`name` AS `name`,`r`.`title` AS `title`,`r`.`status` AS `status`,`r`.`condition` AS `condition`,`m`.`title` AS `menu` from (`dh2y_auth_rule` `r` left join `dh2y_menu` `m` on((`r`.`mid` = `m`.`id`))) ;
